package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import kotlin.math.*

class MainActivity : AppCompatActivity() {

    private lateinit var resultTW: TextView
    private lateinit var resultTW2: TextView // ეს ოპერაციის საჩვენებლად

    private var operand: Double = 0.0
    private var operation: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        resultTW = findViewById((R.id.resultTW))
        resultTW2 = findViewById((R.id.resultTW2))

    }

    fun numberClick(clickedView: View) {

        if(clickedView is TextView) {

            var result = resultTW.text.toString()
            val result1 = resultTW2.text.toString()
            val number = clickedView.text.toString()

            if (result == "0") {
                result = ""
            }

            resultTW.text = result + number

            resultTW2.text = result1 + number

        }

    }

    fun operationClick(clickedView: View) {

        if (clickedView is TextView) {

            val result = resultTW.text.toString()

            if (result.isNotEmpty()) {
                operand = result.toDouble()
            }

            operation = clickedView.text.toString()

            resultTW.text = ""

            var oper = operation

            if (oper == "xⁿ") { // ახარისხების დროს xⁿ რო არ დაწეროს
                oper = "^"
            }

            resultTW2.text = resultTW2.text.toString() + oper

        }

    }

    fun equalsClick(clickedView: View) {

        if(resultTW.text.isNotEmpty()) {

            val secOperandText = resultTW.text.toString()
            var secOperand = 0.0

            if (secOperandText.isNotEmpty()) {
                secOperand = secOperandText.toDouble()
            }

            when(operation) {

                "+" -> resultTW.text = (operand + secOperand).toString()
                "-" -> resultTW.text = (operand - secOperand).toString()
                "*" -> resultTW.text = (operand * secOperand).toString()
                "%" -> resultTW.text = ((operand * secOperand) / 100).toString() // პროცენტი და ახარისხებაც ჩავამატე ბარემ
                "xⁿ" -> resultTW.text = operand.pow(secOperand).toString()
                "/" -> {
                    if (secOperandText == "0") { // ნოლზე გაყოფის ერორი
                        Toast.makeText(applicationContext, "Can't divide by zero.", Toast.LENGTH_SHORT).show()
                    } else {
                        resultTW.text = (operand / secOperand).toString()
                    }
                }

            }

            val res = resultTW.text.toString()
            val len = res.length

            if (res[len - 1] == '0' && res[len - 2] == '.') { // თუ ბოლო ორი სიმბოლო ".0"-ია მოაშლის. :D
                resultTW.text = res.dropLast(2)
            }

            resultTW2.text = ""

        }

    }

    fun floatClick(clickedView: View) { // წერტილის ფუნქცია

        val result = resultTW.text.toString()

        if("." !in result && result.isNotEmpty()) {
            resultTW.text = "$result."
        }

    }

    fun clearClick(clickedView: View) { // Clear ფუნქცია

        resultTW.text = ""
        resultTW2.text = ""

    }

    fun delClick(clickedView: View) { // Del ფუნქცია

        resultTW.text = resultTW.text.toString().dropLast(1)
        resultTW2.text = resultTW2.text.toString().dropLast(1)

    }


    fun otherOperationClick(clickedView: View) {

        if (clickedView is TextView) {

            val result = resultTW.text.toString()

            if (result.isNotEmpty()) {

                if (resultTW2.text.toString().isNotEmpty() && result != resultTW2.text.toString()) {
                    Toast.makeText(applicationContext, "Press equals first.", Toast.LENGTH_SHORT).show()
                } else {

                    when (clickedView.text.toString()) {

                        "|x|" -> resultTW.text = abs(result.toDouble()).toString()
                        "√" -> resultTW.text = sqrt(result.toDouble()).toString()
                        "lg" -> resultTW.text = log10(result.toDouble()).toString()
                        "ln" -> resultTW.text = ln(result.toDouble()).toString()
                    }

                    resultTW2.text = ""

                }

            } else { // თუ რიცხვი არ შეუყვანია მომხმარებელს და ისე აპირებს ოპერაციის შესრულებას.
                Toast.makeText(applicationContext, "Enter number.", Toast.LENGTH_SHORT).show()
            }

        }

    }

    override fun onSaveInstanceState(outState: Bundle) { // ეს ამოტრიალების შემდეგ, უკვე ჩაწერილი რიცხვები რო არ წაიშალოს.
        super.onSaveInstanceState(outState)

        val ress = resultTW.text.toString()
        val ress1 = resultTW2.text.toString()

        outState.putString("res", ress)
        outState.putString("res1", ress1)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        val ress = savedInstanceState.getString("res", 0.toString())
        val ress1 = savedInstanceState.getString("res1", 0.toString())
        resultTW.text = ress
        resultTW2.text = ress1
    }

}